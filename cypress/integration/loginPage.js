describe('Login Page', () => {
    const userName = 'standard_user';
    const userPass = 'secret_sauce';

    before(() => {
        cy.visit('https://www.saucedemo.com/');  
    });

    it('Verify User can login', () => {
        cy.get('[data-test=username]').should('be.visible').type(userName);
        cy.get('[data-test=password]').should('be.visible').type(userPass + '{enter}');
        cy.get('.product_label').should('be.visible')
    });
});




